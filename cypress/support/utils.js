import validUser from "/cypress/fixtures/validUser.json";

export function startReg() {
  cy.get(".header-bottom-nav__list .icon-and-text-link").eq(3).click(); //нажатие на кнопку "Вход"
  cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
    validUser.not_registered_phone
  ); //поле для ввода номера телефона
  cy.wait(2000); //ожидание
  cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
  cy.wait(2000); //ожидание
  cy.get(".el-form-item.is-required.asterisk-left .el-input__inner")
    .eq(0)
    .type(validUser.valid_code); //вводим код из смс
  cy.get(".el-checkbox__inner").click(); //нажимаем на чек-бокс согласие на обработку данных
  cy.get(".sign-up-box__form-controls .el-button.el-button--primary").click(); //нажимаем на кнопку зарегистрироваться
} //TODO стартовые шаги для регистрации пользователя

export function authorization() {
  cy.get(".header-bottom-nav__list .icon-and-text-link").eq(3).click(); //нажатие на кнопку "Вход" для авторизации
  cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
    validUser.registered_phone
  ); //поле для ввода номера телефона
  cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
  cy.wait(2000); //ожидание
  cy.get(".auth-box__body.content .el-input").type(validUser.valid_code); //поле для ввода кода из смс
  cy.get(".el-avatar.el-avatar--circle.hidden-xl-and-down")
    .should("exist")
    .should("be.visible"); //отображение главной страницы авторизованного пользователя
} //TODO авторизация пользователя

export function deleteuser() {
  cy.visit("https://testadmin.pricehack.ru/login/?next=/");
  cy.get(".content #id_password").type(validUser.username_admin); // вводим логин
  cy.get(".content #id_username").type(validUser.password_admin); // вводим пароль
  cy.get('[type="submit"]').click();
  cy.get('.model-user [href="/auth/user/"]').eq(0).click();
  cy.contains("+79777777777").click();
  cy.get(".submit-row .deletelink").click();
  cy.get('#content [type="submit"]').click();
} //TODO удалить зарегистрированного пользователя из админки

export function AddRecipient() {
  cy.get(
    ".checkout-customer-info-customer__block .el-button.is-plain.hidden-md-and-down"
  ).click(); //нажать на кнопку "Добавить получателя"
  cy.get(".checkout-customer-info-add-customer-dialog__main .el-input")
    .eq(0)
    .type("Петр"); //вводим данные в поле "Имя"
  cy.get(".checkout-customer-info-add-customer-dialog__main .el-input")
    .eq(1)
    .type("Петров"); //вводим данные в поле "Фамилия"
  cy.get(".checkout-customer-info-add-customer-dialog__main .el-input")
    .eq(2)
    .type("9999999999"); //вводим данные в поле "Имя"
  cy.get(
    ".el-button.el-button--primary.el-button--large.checkout-customer-info-add-customer-dialog__save"
  ).click(); //нажимаем на кнопку "Сохранить"
} //TODO Добавить получателя в блоке "Оформление заказа"

export function ChangeDataRecipient() {
  cy.get(".checkout-customer-info-customer__block .el-button.is-text").click(); //нажать на кнопку "Изменить"
  cy.get(
    ".checkout-customer-info-change-customer-dialog__radio-controls .el-button.is-circle.is-text"
  )
    .eq(0)
    .click(); //нажать на кнопку "Изменить получателя"
  cy.contains("Петр").should("be.visible"); //отображение данных получателя
  cy.contains("Петров").should("be.visible"); //отображение данных получателя
  cy.contains("+79999999999").should("be.visible"); //отображение данных получателя
  cy.get(".checkout-customer-info-add-customer-dialog__main .el-input")
    .eq(0)
    .clear();
  cy.get(".checkout-customer-info-add-customer-dialog__main .el-input")
    .eq(1)
    .clear();
  cy.get(".checkout-customer-info-add-customer-dialog__main .el-input")
    .eq(2)
    .clear();
  cy.get(".checkout-customer-info-add-customer-dialog__main .el-input")
    .eq(0)
    .type("Василий"); //вводим данные в поле "Имя"
  cy.get(".checkout-customer-info-add-customer-dialog__main .el-input")
    .eq(1)
    .type("Васильев"); //вводим данные в поле "Фамилия"
  cy.get(".checkout-customer-info-add-customer-dialog__main .el-input")
    .eq(2)
    .type("9999999998"); //вводим данные в поле "Имя"
  cy.get(
    ".el-button.el-button--primary.el-button--large.checkout-customer-info-add-customer-dialog__save"
  ).click(); //нажимаем на кнопку "Сохранить"
  cy.contains("Василий").should("be.visible"); //отображение измененных данных получателя
  cy.contains("Васильев").should("be.visible"); //отображение измененных данных получателя
  cy.contains("+79999999998").should("be.visible"); //отображение измененных данных получателя
  cy.get(".checkout-customer-info-change-customer-dialog__close").click(); //закрыть окно
} //TODO Изменить данные получателя в блоке "Оформление заказа"

export function AddAdditionalRecipient() {
  cy.get(".checkout-customer-info-customer__block .el-button.is-text").click(); //нажать на кнопку "Изменить"
  cy.get(
    ".el-button.el-button--large.is-text.checkout-customer-info-change-customer-dialog__add"
  ).click(); //нажимаем на кнопку "Добавить получателя"
  cy.get(".checkout-customer-info-add-customer-dialog__main .el-input")
    .eq(0)
    .type("Василий"); //вводим данные в поле "Имя"
  cy.get(".checkout-customer-info-add-customer-dialog__main .el-input")
    .eq(1)
    .type("Васильев"); //вводим данные в поле "Фамилия"
  cy.get(".checkout-customer-info-add-customer-dialog__main .el-input")
    .eq(2)
    .type("9999999998"); //вводим данные в поле "Имя"
  cy.get(
    ".el-button.el-button--primary.el-button--large.checkout-customer-info-add-customer-dialog__save"
  ).click(); //нажимаем на кнопку "Сохранить"
  cy.contains("Петр Петров").should("be.visible"); //отображение данных получателя
  cy.contains("Василий Васильев").should("be.visible"); //отображение данных получателя
  cy.get(".checkout-customer-info-change-customer-dialog__close").click(); //закрыть окно
} //TODO Добавить дополнительного получателя в блоке "Оформление заказа"

export function DeleteRecipient() {
  cy.get(".checkout-customer-info-customer__block .el-button.is-text").click(); //нажать на кнопку "Изменить"
  cy.get(
    ".checkout-customer-info-change-customer-dialog__radio-controls .el-button.is-circle.is-text"
  )
    .eq(1)
    .click(); //нажать на кнопку "Удалить"
  cy.get(
    ".profile-recipients-delete-dialog__controls .el-button.el-button--primary"
  ).click(); // удалить получателя
  cy.get(".checkout-customer-info-change-customer-dialog__close").click(); //закрыть окно
} //TODO Удалить получателя в блоке "Оформление заказа"

export function AddAddress() {
  cy.get(".checkout-customer-info-address__controls").click(); //нажимаем на кнопку "Добавить адрес"
  cy.get(".col-12.d-none.d-lg-block .el-form-item__content").type(
    "Москва,Ленинский проспект"
  ); //ввести адрес
  cy.get(
    '.p-dropdown-items-wrapper [aria-label="Москва, Ленинский проспект"]'
  ).click();
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(1)
    .type("1"); //ввести Квартира/Офис
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(4)
    .type("2"); //ввести номер этажа
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(5)
    .type("3"); // ввести номер подъезда
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(6)
    .type("4"); //ввести номер домофона
  cy.get(".el-textarea__inner").type("тест"); //ввести комментарии курьера
  cy.get(".col-12.d-none.d-lg-block .el-form-item__content").click();
  cy.get(".container-fluid .el-button.el-button--primary").click(); //нажать на кнопку сохранить
} //TODO Добавить адрес в блоке "Оформление заказа"

export function DeleteAddress() {
  cy.get(
    ".checkout-customer-info-address__title-block.d-flex.align-items-center .el-button.is-text"
  ).click(); //нажать на кнопку изменить
  cy.get(
    ".checkout-customer-info-change-address-dialog__radio-controls .el-button.is-circle.is-text"
  )
    .eq(1)
    .click(); //нажать на кнопку "Удалить"
  cy.get(
    ".profile-addresses-delete-dialog__controls .el-button.el-button--primary"
  ).click(); //удалить адрес получателя
  cy.get(".el-button.el-button--small.is-circle.is-text").click(); //закрыть окно
} //TODO Удалить адрес в блоке "Оформление заказа"

export function ChangeDataAddress() {
  cy.get(
    ".checkout-customer-info-address__title-block.d-flex.align-items-center .el-button.is-text"
  ).click(); //нажать на кнопку изменить
  cy.contains(
    "Москва, Ленинский проспект, кв. 1, этаж 2, подъезд 3, домофон 4"
  ).should("be.visible"); //отображение данных адреса
  cy.get(
    ".checkout-customer-info-change-address-dialog__radio-controls .el-button.is-circle.is-text"
  )
    .eq(0)
    .click(); //нажать на кнопку "Изменить"
  cy.get(".col-12.d-none.d-lg-block .el-form-item__content").clear(); //очистить адрес
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(1)
    .clear(); //очистить квартира/офис
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(4)
    .clear(); //очистить номер этажа
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(5)
    .clear(); //очистить номер подъезда
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(6)
    .clear(); //очистить номер домофона
  cy.get(".el-textarea__inner").clear(); // очистить комментарии курьера
  cy.get(".col-12.d-none.d-lg-block .el-form-item__content").type(
    "Санкт-Петербург,Невский проспект"
  ); //ввести адрес
  cy.get(
    '.p-dropdown-items-wrapper [aria-label="Санкт-Петербург, Невский проспект"]'
  ).click();
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(1)
    .type("5"); //ввести Квартира/Офис
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(4)
    .type("6"); //ввести номер этажа
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(5)
    .type("7"); // ввести номер подъезда
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(6)
    .type("8"); //ввести номер домофона
  cy.get(".el-textarea__inner").type("новый тест"); //ввести комментарии курьера
  cy.get(".col-12.d-none.d-lg-block .el-form-item__content").click();
  cy.get(".container-fluid .el-button.el-button--primary").click(); //нажать на кнопку сохранить
  cy.get(".el-button.el-button--small.is-circle.is-text").click(); //закрыть окно
  cy.contains(
    "Санкт-Петербург, Невский проспект, кв. 5, этаж 6, подъезд 7, домофон 8"
  ).should("be.visible"); //отображение данных адреса
} //TODO Изменить данные адреса в блоке "Оформление заказа"

export function AddAdditionalAddress() {
  cy.get(
    ".checkout-customer-info-address__title-block.d-flex.align-items-center .el-button.is-text"
  ).click(); //нажать на кнопку изменить
  cy.get(
    ".el-button.el-button--large.is-text.checkout-customer-info-change-address-dialog__add"
  ).click(); //нажать на кнопку "Добавить адрес"
  cy.get(".col-12.d-none.d-lg-block .el-form-item__content").type(
    "Санкт-Петербург,Невский проспект"
  ); //ввести адрес
  cy.get(
    '.p-dropdown-items-wrapper [aria-label="Санкт-Петербург, Невский проспект"]'
  ).click();
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(1)
    .type("5"); //ввести Квартира/Офис
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(4)
    .type("6"); //ввести номер этажа
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(5)
    .type("7"); // ввести номер подъезда
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(6)
    .type("8"); //ввести номер домофона
  cy.get(".el-textarea__inner").type("новый тест"); //ввести комментарии курьера
  cy.get(".col-12.d-none.d-lg-block .el-form-item__content").click();
  cy.get(
    ".checkout-customer-info-add-address-drawer-panel__controls .el-button.el-button--primary"
  ).click(); //нажать на кнопку сохранить
  cy.contains(
    "Санкт-Петербург, Невский проспект, кв. 5, этаж 6, подъезд 7, домофон 8"
  ).should("be.visible"); //отображение данных адреса
  cy.contains(
    "Москва, Ленинский проспект, кв. 1, этаж 2, подъезд 3, домофон 4"
  ).should("be.visible"); //отображение данных адреса
  cy.get(".checkout-customer-info-change-address-dialog__close").click(); //закрыть окно
} //TODO Добавить дополнительный адрес в блоке "Оформление заказа"

export function AddAddressProfile() {
  cy.get(".user-menu__item .color_text-secondary.text-m").eq(5).click(); //нажать на адреса
  cy.get(".profile-addresses__title-block .el-button.is-plain").click(); //нажать добавить адрес
  cy.get(".col-12.d-none.d-lg-block .el-form-item__content").type(
    "Москва,Ленинский проспект"
  ); //ввести адрес
  cy.get(
    '.p-dropdown-items-wrapper [aria-label="Москва, Ленинский проспект"]'
  ).click();
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(1)
    .type("1"); //ввести Квартира/Офис
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(4)
    .type("2"); //ввести номер этажа
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(5)
    .type("3"); // ввести номер подъезда
  cy.get(".el-form-item.is-no-asterisk.asterisk-left .el-input__wrapper")
    .eq(6)
    .type("4"); //ввести номер домофона
  cy.get(".el-textarea__inner").type("тест"); //ввести комментарии курьера
  cy.get(".col-12.d-none.d-lg-block .el-form-item__content").click();
  cy.get(".container-fluid .el-button.el-button--primary").click(); //нажать на кнопку сохранить
}

export function DeleteProductBasket() {
  cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
  cy.wait(2000); //ожидание
  cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
  cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
} //TODO Удалить товары из корзины

export function ConfirmGeolocation() {
  cy.get(".el-popper.is-light.el-popover.location-popover").then(($element) => {
    if ($element.length > 0) {
      // Элемент найден
      cy.log("Элемент найден");
    } else {
      // Элемент не найден
      cy.log("Элемент не найден");
    }
    cy.get(".el-popper.is-light.el-popover.location-popover")
      .should("be.visible")
      .then(($element) => {
        // Проверка наличия кнопки в элементе
        if ($element.find(".el-button.el-button--primary").length > 0) {
          // Нажатие на кнопку
          cy.wait(2000); //ожидание
          cy.get(
            ".el-popper.is-light.el-popover.location-popover .el-button.el-button--primary"
          ).click();
        } else {
          cy.log("Кнопка не найдена");
        }
      });
  });
} //TODO в сплывающем окне подтвердить геолакацию

export function AddProductToBasket(cnt) {
  for (let i = 0; i < cnt; i++) {
    cy.get(".el-card .product-card__controls .el-button.el-button--primary")
      .eq(i)
      .click();
  }
} //TODO добавляем товар из каталога в корзину

export function IsVisibleProductBasket(cnt) {
  for (let i = 0; i < cnt; i++) {
    cy.get(".basket-item").eq(i).should("exist").should("be.visible");
  }
} //TODO выбранный товар из каталога добавлен и видимый в корзине

export function IsVisibleProductCheckout(cnt) {
  for (let i = 0; i < cnt; i++) {
    cy.get(".checkout-items .checkout-item")
      .eq(i)
      .should("exist")
      .should("be.visible");
  }
} //TODO выбранный товар из корзины добавлен и видимый в оформлении заказа

export function AddProductToFavorite(cnt) {
  for (let i = 0; i < cnt; i++) {
    cy.get(".el-button.is-circle.is-text.product-card__favorite").eq(i).click();
  }
} //TODO добавляем товар из каталога в избранное

export function IsVisibleProductFavorite(cnt) {
  for (let i = 0; i < cnt; i++) {
    cy.get(".el-card__body").eq(i).should("exist").should("be.visible");
  }
} //TODO товар из каталога добавлен и видимый в избранном

export function ClickProduct(cnt) {
  for (let i = 0; i < cnt; i++) {
    cy.get(".products__list .el-card__body").eq(i).click();
  }
} //TODO нажать на товар в каталоге
