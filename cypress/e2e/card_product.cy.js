import validUser from "/cypress/fixtures/validUser.json";
import { authorization } from "/cypress/support/utils";
import { ClickProduct } from "/cypress/support/utils";
import { IsVisibleProductBasket } from "/cypress/support/utils";
import { IsVisibleProductFavorite } from "/cypress/support/utils";
import { ConfirmGeolocation } from "../support/utils";
describe("Страница карточки товаров'", () => {
  beforeEach(() => {
    cy.visit("/catalog/vse-tovaryi");
  });

  it("Test 1: Товар успешно добавляется в корзину(не авторизован)", () => {
    cy.wait(2000); //ожидание
    ConfirmGeolocation();
    ClickProduct(1); // находим товар и кликаем по нему
    cy.wait(2000); //ожидание
    cy.get(
      ".product-section__main-block .el-button.el-button--primary"
    ).click(); // на странице карточки товара кликаем на "В корзину"
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    IsVisibleProductBasket(1); // находим товар, который добавляли и проверяем что он видимый и есть в корзине
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
  });
  it("Test 2: Товар успешно добавляется в корзину(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.wait(2000); //ожидание
    ClickProduct(1); // находим товар и кликаем по нему
    cy.wait(2000); //ожидание
    cy.get(
      ".product-section__main-block .el-button.el-button--primary"
    ).click(); // на странице карточки товара кликаем на "В корзину"
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    IsVisibleProductBasket(1); // находим товар, который добавляли и проверяем что он видимый и есть в корзине
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
  });
  it("Test 3: При добавлении товара в корзину, добавляется количества товара (не авторизован)", () => {
    cy.wait(2000); //ожидание
    ClickProduct(1); // находим товар и кликаем по нему
    cy.wait(2000); //ожидание
    cy.get(
      ".product-section__main-block .el-button.el-button--primary"
    ).click(); // на странице карточки товара кликаем на "В корзину"
    cy.get(".el-input-number__increase").eq(2).click(); // нажимаем + и добавляем 2 товара в корзину
    cy.get('.el-input__inner[type="number"]')
      .eq(2)
      .should("exist")
      .should("be.visible")
      .should("have.attr", "aria-valuenow", "2"); // проверяем, что количество товара изменилось на 2
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    IsVisibleProductBasket(1); // находим товар, который добавляли и проверяем что он видимый и есть в корзине
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
  });
  it("Test 4: При добавлении товара в корзину, добавляется количества товара (авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.wait(2000); //ожидание
    ClickProduct(1); // находим товар и кликаем по нему
    cy.wait(2000); //ожидание
    cy.get(
      ".product-section__main-block .el-button.el-button--primary"
    ).click(); // на странице карточки товара кликаем на "В корзину"
    cy.get(".el-input-number__increase").eq(2).click(); // нажимаем + и добавляем 2 товара в корзину
    cy.get('.el-input__inner[type="number"]')
      .eq(2)
      .should("exist")
      .should("be.visible")
      .should("have.attr", "aria-valuenow", "2"); // проверяем, что количество товара изменилось на 2
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    IsVisibleProductBasket(1); // находим товар, который добавляли и проверяем что он видимый и есть в корзине
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
  });

  it("Test 5: Товар добавляется в избранное(не авторизован)", () => {
    cy.wait(2000); //ожидание
    ClickProduct(1); // находим товар и кликаем по нему
    cy.wait(2000); //ожидание
    ConfirmGeolocation();
    cy.get(
      ".product-buy-block__controls-top .product-buy-block__controls-right"
    ).click(); //добавляем товар в избранное

    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    cy.wait(2000); //ожидание
    IsVisibleProductFavorite(1); // находим товар, который добавляли и проверяем что он видимый и есть в избранном
    cy.get(".el-button.is-circle.is-text.product-card__favorite.is-active")
      .eq(0)
      .click(); // отменяем товар из избранного
    cy.reload(); //обновляем страницу
    cy.contains("В избранном пусто").should("be.visible"); // проверяем, что в избранном пусто
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
  });

  it("Test 6: Товар добавляется в избранное(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.wait(2000); //ожидание
    ClickProduct(1); // находим товар и кликаем по нему
    cy.wait(2000); //ожидание
    ConfirmGeolocation();
    cy.get(
      ".product-buy-block__controls-top .product-buy-block__controls-right"
    ).click(); //добавляем товар в избранное
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    cy.wait(2000); //ожидание
    IsVisibleProductFavorite(1); // находим товар, который добавляли и проверяем что он видимый и есть в избранном
    cy.get(".el-button.is-circle.is-text.product-card__favorite.is-active")
      .eq(0)
      .click(); // отменяем товар из избранного
    cy.reload(); //обновляем страницу
    cy.contains("В избранном пусто").should("be.visible"); // проверяем, что в избранном пусто
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
  });
  it("Test 13: Открыть отзывы товара(не авторизован)", () => {
    cy.wait(2000); //ожидание
    ClickProduct(1); // находим товар и кликаем по нему
    cy.wait(2000); //ожидание
    cy.get("section.product-section .product-section__top-rating")
      .eq(0)
      .click(); //нажимаем на кнопку отзыва
    cy.get(".product-info-section-feedbacks-card .content")
      .contains("Достоинства проверка")
      .should("be.visible");
    cy.get(".product-info-section-feedbacks-card .content")
      .contains("Недостатки проверка")
      .should("be.visible"); //находим отзыв по наименованию и проверяем, что он видимый
    cy.get(".product-info-section-feedbacks-card .content")
      .contains("Комментарии проверка")
      .should("be.visible"); //находим отзыв по наименованию и проверяем, что он видимый
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
  });
  it("Test 14: Открыть отзывы товара(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog");
    cy.wait(2000); //ожидание
    ClickProduct(1); // находим товар и кликаем по нему
    cy.wait(2000); //ожидание
    cy.get("section.product-section .product-section__top-rating")
      .eq(0)
      .click(); //нажимаем на кнопку отзыва
    cy.get(".product-info-section-feedbacks-card .content")
      .contains("Достоинства проверка")
      .should("be.visible");
    cy.get(".product-info-section-feedbacks-card .content")
      .contains("Недостатки проверка")
      .should("be.visible"); //находим отзыв по наименованию и проверяем, что он видимый
    cy.get(".product-info-section-feedbacks-card .content")
      .contains("Комментарии проверка")
      .should("be.visible"); //находим отзыв по наименованию и проверяем, что он видимый
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
  });
  it("Test 15: Открыть Вопросы о товаре(не авторизован)", () => {
    cy.wait(2000); //ожидание
    ClickProduct(1); // находим товар и кликаем по нему
    cy.wait(2000); //ожидание
    cy.get("section.product-section .product-section__top-questions")
      .eq(0)
      .click(); //нажимаем на кнопку вопрос о товаре
    cy.get(".el-tab-pane .product-info-section-questions-form__title")
      .should("exist")
      .should("be.visible"); //находим заголовок раздела и проверяем, что он видимый
    cy.get(".el-form.el-form--default .el-textarea__inner").should(
      "be.visible"
    ); // проверяем, что есть поле ввода самого вопроса
    cy.get(".el-tab-pane .el-button.el-button--primary"); // проверяем, что есть кнопка с текстом "Отправить" и она видимая для пользователя
  });
  it("Test 16: Открыть Вопросы о товаре(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog");
    cy.wait(2000); //ожидание
    ClickProduct(1); // находим товар и кликаем по нему
    cy.get("section.product-section .product-section__top-questions")
      .eq(0)
      .click(); //нажимаем на кнопку вопрос о товаре
    cy.get(".el-tab-pane .product-info-section-questions-form__title")
      .should("exist")
      .should("be.visible"); //находим заголовок раздела и проверяем, что он видимый
    cy.get(".el-form.el-form--default .el-textarea__inner").should(
      "be.visible"
    ); // проверяем, что есть поле ввода самого вопроса
    cy.get(".el-tab-pane .el-button.el-button--primary"); // проверяем, что есть кнопка с текстом "Отправить" и она видимая для пользователя
  });
});
