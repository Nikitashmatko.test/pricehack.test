import validUser from "/cypress/fixtures/validUser.json";
import { authorization } from "/cypress/support/utils";
import { AddProductToFavorite } from "/cypress/support/utils";
import { IsVisibleProductFavorite } from "/cypress/support/utils";
describe("", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1: Избранное в списке заказа", () => {
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    AddProductToFavorite(1); //добавить товар в избранное
    cy.get('.icon-and-text-link[href="/orders"]').click(); //нажать на кнопку заказы
    cy.get('.color_text-secondary.text-m[href="/favorite"').click(); //нажать на кнопку избранное
    IsVisibleProductFavorite(1); //товар отображается в избранном
    cy.get(".el-button.is-circle.is-text.product-card__favorite.is-active")
      .eq(0)
      .click(); // удаляем товар из избранного
    cy.reload(); //обновляем страницу
    cy.contains("В избранном пусто").should("be.visible"); // проверяем, что в избранном пусто
  });
});
