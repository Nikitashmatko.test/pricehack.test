import { authorization } from "/cypress/support/utils";
import { AddProductToBasket } from "/cypress/support/utils";
import { AddRecipient } from "/cypress/support/utils";
import { DeleteRecipient } from "/cypress/support/utils";
import { DeleteProductBasket } from "/cypress/support/utils";
import { ChangeDataRecipient } from "/cypress/support/utils";
import { AddAddress } from "/cypress/support/utils";
import { DeleteAddress } from "/cypress/support/utils";
import { ChangeDataAddress } from "/cypress/support/utils";
import { AddAdditionalRecipient } from "/cypress/support/utils";
import { AddAdditionalAddress } from "/cypress/support/utils";
import { ConfirmGeolocation } from "../support/utils";
describe("Страница оформление заказа'", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1: Добавить получателя ", () => {
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    AddProductToBasket(1); //добавляем 1 позиции товара в корзину из каталога
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    cy.get(
      ".basket-section-summary .el-button.el-button--primary.el-button--large"
    ).click(); //нажать на кнопку "Оформление заказа"
    cy.url().should("eq", "https://test.pricehack.ru/checkout"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    AddRecipient(); //Добавляем получателя
    DeleteRecipient(); //Удалить получателя для следующего теста
    DeleteProductBasket(); //Удаляем товары из корзины
  });

  it("Test 2 : Изменить данные получателя ", () => {
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    AddProductToBasket(1); //добавляем 1 позиции товара в корзину из каталога
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    cy.get(
      ".basket-section-summary .el-button.el-button--primary.el-button--large"
    ).click(); //нажать на кнопку "Оформление заказа"
    cy.url().should("eq", "https://test.pricehack.ru/checkout"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    AddRecipient(); //Добавляем получателя
    ChangeDataRecipient(); //Изменить данные получателя
    DeleteRecipient(); //Удалить получателя для следующего теста
    DeleteProductBasket(); //Удаляем товары из корзины
  });

  it("Test 3: Добавить адрес ", () => {
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    AddProductToBasket(1); //добавляем 1 позиции товара в корзину из каталога
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    cy.get(
      ".basket-section-summary .el-button.el-button--primary.el-button--large"
    ).click(); //нажать на кнопку "Оформление заказа"
    cy.url().should("eq", "https://test.pricehack.ru/checkout"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    AddAddress(); //добавляем адрес получателя
    DeleteAddress(); //удалить адрес получателя для следующего теста
    DeleteProductBasket(); //Удаляем товары из корзины
  });

  it("Test 4: Изменить адрес доставки ", () => {
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    AddProductToBasket(1); //добавляем 1 позиции товара в корзину из каталога
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    cy.get(
      ".basket-section-summary .el-button.el-button--primary.el-button--large"
    ).click(); //нажать на кнопку "Оформление заказа"
    cy.url().should("eq", "https://test.pricehack.ru/checkout"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    AddAddress(); //добавляем адрес получателя
    ChangeDataAddress(); //изменить адрес
    DeleteAddress(); //удалить адрес получателя для следующего теста
    DeleteProductBasket(); //Удаляем товары из корзины
  });

  it("Test 5: Добавить дополнительного получателя ", () => {
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    AddProductToBasket(1); //добавляем 1 позиции товара в корзину из каталога
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    cy.get(
      ".basket-section-summary .el-button.el-button--primary.el-button--large"
    ).click(); //нажать на кнопку "Оформление заказа"
    cy.url().should("eq", "https://test.pricehack.ru/checkout"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    AddRecipient(); //Добавляем получателя
    AddAdditionalRecipient(); //Добавить дополнительного получателя
    DeleteRecipient(); //Удалить получателя для следующего теста
    DeleteRecipient(); //Удалить получателя для следующего теста
    DeleteProductBasket(); //Удаляем товары из корзины
  });

  it("Test 6: Добавить дополнительный адрес ", () => {
    // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    AddProductToBasket(1); //добавляем 1 позиции товара в корзину из каталога
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    cy.get(
      ".basket-section-summary .el-button.el-button--primary.el-button--large"
    ).click(); //нажать на кнопку "Оформление заказа"
    cy.url().should("eq", "https://test.pricehack.ru/checkout"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    AddAddress(); //добавляем адрес получателя
    AddAdditionalAddress();
    DeleteAddress(); //удалить адрес получателя для следующего теста
    DeleteAddress(); //удалить адрес получателя для следующего теста
    DeleteProductBasket(); //Удаляем товары из корзины
  });
});
