import validUser from "/cypress/fixtures/validUser.json";
import { startReg } from "/cypress/support/utils";
import { deleteuser } from "/cypress/support/utils";

describe("Страница раздела 'Регистрация пользователя'", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1: Регистрация физлица при вводе валидных данных", () => {
    startReg(); //стартовые шаги для регистрации
    cy.wait(3000); //ожидание
    cy.get(".el-form.el-form--default .el-input").eq(0).type("Иван"); //вводим данные в поле "Имя"
    cy.get(".el-form.el-form--default .el-input").eq(1).type("Иванов"); //водим данные в поле "Фамилия"
    cy.get(".el-form.el-form--default .el-input").eq(2).type("ivan@mail.ru"); //вводим данные в поле "e-mail"
    cy.get(".el-form.el-form--default .el-button.el-button--primary").click(); //нажать на кнопку "Продолжить@
    cy.wait(2000); //ожидание
    cy.get(".el-form-item__content .el-input").type(validUser.valid_code); //вводим код из e-mail
    deleteuser();
  });
  it("Test 2: Выбор кода страны при регистрации/авторизации", () => {
    cy.get(".header-bottom-nav__list .icon-and-text-link").eq(3).click(); //нажатие на кнопку "Вход" для регистрации
    cy.get(".el-form.el-form--default .el-input__wrapper").eq(0).click(); //нажатие на кнопку выбрать код страны
    cy.contains("Катар(+974)").click(); //выбор кода страны
    cy.get(".country-phone-input__code")
      .should("be.visible")
      .should("exist")
      .should("have.text", "+974"); //отображение выбранного кода
  });
  it("Test 3: Регистрация Физлица при вводе некорректного номера телефона", () => {
    cy.get(".header-bottom-nav__list .icon-and-text-link").eq(3).click(); //нажатие на кнопку "Вход" для регистрации
    cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
      "988888888"
    ); //поле для ввода номера телефона
    cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
    cy.get(".el-form-item__error")
      .should("be.visible")
      .contains("Длинна номера 12 символов"); //отображение ошибки "Длинна номера 12 символов"
  });
  it("Test 4: Изменение номера телефона на странице регистрации физлица", () => {
    cy.get(".header-bottom-nav__list .icon-and-text-link").eq(3).click(); //нажатие на кнопку "Вход"
    cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
      validUser.not_registered_phone
    ); //поле для ввода номера телефона
    cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
    cy.get(".box__footer .el-button.is-text").click(); //нажатие на кнопку "Изменить номер телефона"
    cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
      "9001112233"
    ); //поле для ввода номера телефона
    cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
    cy.get("span[data-v-bc01283e]")
      .eq(0)
      .should("have.text", "+7 (900) 111 - 22 - 33"); //отображение измененного номера на следующем шаге
  });
  it("Test 5: Регистрация физлица не отмечая чек-бокс 'Согласен с условиями пользовательского соглашения и условиями обработки персональных данных'", () => {
    cy.get(".header-bottom-nav__list .icon-and-text-link").eq(3).click(); //нажатие на кнопку "Вход"
    cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
      validUser.not_registered_phone
    ); //поле для ввода номера телефона
    cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
    cy.wait(3000); //ожидание
    cy.get(".el-form-item.is-required.asterisk-left .el-input__inner")
      .eq(0)
      .type(validUser.valid_code); //вводим код из смс
    cy.get(".sign-up-box__form-controls .el-button.el-button--primary").click(); //нажимаем на кнопку зарегистрироваться
    cy.wait(2000); //ожидание
    cy.get(".el-checkbox__inner")
      .invoke("css", "color") // получаем значение свойства "color" данного элемента
      .then((color) => {
        expect(color).to.deep.equal("rgb(252, 15, 15)"); // получаем значение цвета и передаем его в функцию обратного вызова в качестве аргумента "color" и проверяем, что значение "color" равно строке "rgb(252, 15, 15)"
      });
  });
  it("Test 6: Получение нового кода подтверждения на странице регистрации физлица и успешный переход на следующий шаг", () => {
    cy.get(".header-bottom-nav__list .icon-and-text-link").eq(3).click(); //нажатие на кнопку "Вход"
    cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
      validUser.not_registered_phone
    ); //поле для ввода номера телефона
    cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
    cy.wait(2000); //ожидание
    cy.get(".sign-up-box__form-controls .el-button.el-button--large")
      .eq(1)
      .click()
      .should("have.attr", "aria-disabled", "true"); //нажатие на кнопку "Получить новый код" и отображение таймера
    cy.wait(2000); //ожидание
    cy.get(".el-form-item.is-required.asterisk-left .el-input__inner")
      .eq(0)
      .type(validUser.valid_code); //вводим код из смс
    cy.get(".el-checkbox__inner").click(); //Выбран чек-бокс "Согласие на обработку данных"
    cy.get(".sign-up-box__form-controls .el-button.el-button--primary").click(); //Нажатие на кнопку "Зарегистрироваться"
    cy.contains("Заполните данные профиля");
  });

  it("Test 7: Регистрация  физлица не заполняя данные профиля", () => {
    startReg(); //стартовые шаги для регистрации
    cy.wait(2000); //ожидание
    cy.get(".el-form.el-form--default .el-button.el-button--primary").click(); //нажатие на кнопку "Продолжить"
    cy.get(".el-form-item__error")
      .eq(0)
      .should("be.visible")
      .contains("Обязательное поле"); //отображание ошибки "Обязательное поле"
    cy.get(".el-form-item__error")
      .eq(1)
      .should("be.visible")
      .contains("Обязательное поле"); //отображание ошибки "Обязательное поле"
    cy.get(".el-form-item__error")
      .eq(2)
      .should("be.visible")
      .contains("E-mail не может быть пустым"); //отображание ошибки "E-mail не может быть пустым"
  });

  it("Test 8: Получение нового кода подтверждения для e-mail при регистрации физлица ", () => {
    startReg(); //стартовые шаги для регистрации
    cy.wait(3000); //ожидание
    cy.get(".el-form.el-form--default .el-input").eq(0).type("Иван"); //вводим данные в поле "Имя"
    cy.get(".el-form.el-form--default .el-input").eq(1).type("Иванов"); //водим данные в поле "Фамилия"
    cy.get(".el-form.el-form--default .el-input").eq(2).type("ivan@mail.ru"); //вводим данные в поле "e-mail"
    cy.get(".el-form.el-form--default .el-button.el-button--primary").click(); //нажать на кнопку "Продолжить"
    cy.wait(2000); //ожидание
    cy.get(".el-form.el-form--default .el-button.el-button--large")
      .click()
      .should("have.attr", "aria-disabled", "true"); //нажатие на кнопку "Получить новый код" и отображение таймера
  });

  it("Test 9: Изменить e-mail при регистрации физлица ", () => {
    startReg(); //стартовые шаги для регистрации
    cy.wait(3000); //ожидание
    cy.get(".el-form.el-form--default .el-input").eq(0).type("Иван"); //вводим данные в поле "Имя"
    cy.get(".el-form.el-form--default .el-input").eq(1).type("Иванов"); //водим данные в поле "Фамилия"
    cy.get(".el-form.el-form--default .el-input").eq(2).type("ivan@mail.ru"); //вводим данные в поле "e-mail"
    cy.get(".el-form.el-form--default .el-button.el-button--primary").click(); //нажать на кнопку "Продолжить"
    cy.wait(2000); //ожидание
    cy.get(".sign-up-box__footer .el-button.is-text").click(); //нажатие на кнопку "изменить e-mail"
    cy.wait(2000); //ожидание
    cy.get(".el-input__inner").eq(4).clear();
    cy.get(".el-input__inner").eq(4).type("test2@mail.ru"); //поле для ввода "E-mail"
    cy.get(".el-form.el-form--default .el-button.el-button--primary").click(); //нажать на кнопку "Продолжить"
    cy.contains("test2@mail.ru был отправлен код подтверждения"); //отображение измененного e-mail
  });
  it("Test 12: Регистрация Физлица при вводе некорректного 'e-mail' в окне 'Заполните данные профиля'", () => {
    startReg(); //стартовые шаги для регистрации
    cy.wait(2000); //ожидание
    cy.get(".el-form.el-form--default .el-input").eq(2).type("testemail"); //поле для ввода "E-mail"
    cy.get(".el-form.el-form--default .el-button.el-button--primary").click(); //нажать на кнопку "Продолжить"
    cy.contains("Некорректный E-mail"); //отображение ошибки "Некорректный E-mail"
  });
});
