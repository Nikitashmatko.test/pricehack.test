import validUser from "/cypress/fixtures/validUser.json";

describe("Страница раздела 'Авторизация пользователя'", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 1: Авторизация на сайте при вводе валидных данных", () => {
    cy.get(".header-bottom-nav__list .icon-and-text-link").eq(3).click(); //нажатие на кнопку "Вход" для авторизации
    cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
      validUser.registered_phone
    ); //поле для ввода номера телефона
    cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
    cy.get(".auth-box__body.content .el-input").type(validUser.valid_code); //поле для ввода кода из смс.type(validUser.valid_code); //поле для ввода кода из смс
    cy.get(".el-avatar.el-avatar--circle.hidden-xl-and-down")
      .should("exist")
      .should("be.visible"); //отображение главной страницы авторизованного пользователя
  });
  it("Test 2: Авторизация на сайте при вводе некорректного кода подтверждения телефона", () => {
    cy.get(".header-bottom-nav__list .icon-and-text-link").eq(3).click(); //нажатие на кнопку "Вход" для авторизации
    cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
      validUser.registered_phone
    ); //поле для ввода номера телефона
    cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
    cy.get(".auth-box__body.content .el-input").type("11");
  });
  it("Test 3: Получение нового кода подтверждения на странице авторизации пользователя и успешная авторизация", () => {
    cy.get(".header-bottom-nav__list .icon-and-text-link").eq(3).click(); //нажатие на кнопку "Вход" для авторизации
    cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
      validUser.registered_phone
    ); //поле для ввода номера телефона
    cy.wait(2000); //ожидание
    cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
    cy.get(".auth-box__body.content .el-button.el-button--large")
      .click()
      .should("have.attr", "aria-disabled", "true"); //нажатие на кнопку "Получить новый код" и отображение таймера
    cy.get(".auth-box__body.content .el-input").type(validUser.valid_code); //поле для ввода кода из смс
    cy.get(".el-avatar.el-avatar--circle.hidden-xl-and-down")
      .should("exist")
      .should("be.visible"); //отображение главной страницы авторизованного пользователя
  });
  it("Test 4: Изменение номера телефона на странице авторизации пользователя", () => {
    cy.get(".header-bottom-nav__list .icon-and-text-link").eq(3).click(); //нажатие на кнопку "Вход" для авторизации
    cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
      validUser.registered_phone
    ); //поле для ввода номера телефона
    cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
    cy.get(".box__footer .el-button.is-text").click(); //нажатие на кнопку "Изменит номер"
    cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
      "9888888844"
    ); //поле для ввода номера телефона
    cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
    cy.get(".color_text-secondary.text-s.text-m_lg").should(
      "contain",
      "+7 (988) 888 - 88 - 44"
    ); //отображение измененного номера на следующем шаге
  });
  it("Test 5: Ничего не вводить в поле номер телефона при авторизации пользователя", () => {
    cy.get(".header-bottom-nav__list .icon-and-text-link").eq(3).click(); //нажатие на кнопку "Вход" для авторизации
    cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
    cy.get(".el-form-item__error")
      .should("contain", "Введите номер")
      .should("exist")
      .should("be.visible"); // отображение ошибки "Введите номер"
  });
});
