import validUser from "/cypress/fixtures/validUser.json";
import { authorization } from "/cypress/support/utils";
import { AddProductToBasket } from "/cypress/support/utils";
import { IsVisibleProductBasket } from "../support/utils";
import { IsVisibleProductCheckout } from "../support/utils";
import { ConfirmGeolocation } from "../support/utils";
describe("Страница раздела корзина", () => {
  beforeEach(() => {
    cy.visit("/catalog/vse-tovaryi");
  });
  it("Test 1: На странице Корзина отображается только выбранный товар(не авторизован)", () => {
    AddProductToBasket(2); //добавляем 2 позиции товара в корзину из каталога
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    IsVisibleProductBasket(2); // выбранный товар из каталога добавлен и видимый в корзине
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.wait(2000); //ожидание
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
    // TODO: как добавят в раздел "Корзина" модальное окно "Удалить товар" добавить его в тесты (добавила задержку в 2 секунды,без нее товар не удаляется)
  });
  it("Test 2: На странице Корзина отображается только выбранный товар(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    AddProductToBasket(2); //добавляем 2 позиции товара в корзину из каталога
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    IsVisibleProductBasket(2); // выбранный товар из каталога добавлен и видимый в корзине
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.wait(2000); //ожидание
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
    // TODO: как добавят в раздел "Корзина" модальное окно "Удалить товар" добавить его в тесты (добавила задержку в 2 секунды,без нее товар не удаляется)
  });
  it("Test 3: Все товары удаляются из корзины(не авторизован)", () => {
    AddProductToBasket(2); //добавляем 2 позиции товара в корзину из каталога
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    IsVisibleProductBasket(2); // выбранный товар из каталога добавлен и видимый в корзине
    cy.wait(3000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.wait(2000); //ожидание
    cy.contains("В корзине пока пусто").should("exist").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: как добавят в раздел "Корзина" модальное окно "Удалить товар" добавить его в тесты (добавила задержку в 2 секунды,без нее товар не удаляется)
  it("Test 4: Все товары удаляются из корзины(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    AddProductToBasket(2); //добавляем 2 позиции товара в корзину из каталога
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    IsVisibleProductBasket(2); // выбранный товар из каталога добавлен и видимый в корзине
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.wait(2000); //ожидание
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: как добавят в раздел "Корзина" модальное окно "Удалить товар" добавить его в тесты (добавила задержку в 2 секунды,без нее товар не удаляется)
  it("Test 5: Определенный товар удаляется из корзины при нажатии на кнопку 'Удалить' (не авторизован)", () => {
    AddProductToBasket(2); //добавляем 2 позиции товара в корзину из каталога
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    IsVisibleProductBasket(2); // выбранный товар из каталога добавлен и видимый в корзине
    cy.get(".basket-item__content.content .text-s-medium").eq(1).click(); // удаляем один из товаров
    cy.wait(2000); //ожидание
    IsVisibleProductBasket(0); // проверяем, что в корзине остался один товар
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.wait(3000); //ожидание
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: как добавят в раздел "Корзина" модальное окно "Удалить товар" добавить его в тесты
  it("Test 6: Определенный товар удаляется из корзины при нажатии на кнопку 'Удалить' (авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    AddProductToBasket(2); //добавляем 2 позиции товара в корзину из каталога
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    IsVisibleProductBasket(2); // выбранный товар из каталога добавлен и видимый в корзине
    cy.get(".basket-item__content.content .text-s-medium").eq(1).click(); // удаляем один из товаров
    cy.wait(2000); //ожидание
    IsVisibleProductBasket(0); // проверяем, что в корзине остался один товар
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.wait(2000); //ожидание
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: как добавят в раздел "Корзина" модальное окно "Удалить товар" добавить его в тесты

  it("Test 7: Несколько товаров удяется из корзину при выборе чек-боксов (не авторизован)", () => {
    cy.wait(2000); //ожидание
    AddProductToBasket(3); //добавляем в корзину из каталога 3 позиции товара
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    cy.wait(2000); //ожидание
    cy.get(
      ".basket-section-items__top .el-checkbox.el-checkbox--small.is-checked"
    ).click(); // убираем чек-бокс "Выбрать все"
    cy.wait(4000); //ожидание
    cy.get(".basket-section-items__item .el-checkbox__input").eq(0).click(); //отмечаем чек-боксом  товар
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__item .el-checkbox__input").eq(1).click(); //отмечаем чек-боксом товар
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем выбранные товары из корзины
    cy.wait(2000); //ожидание
    IsVisibleProductBasket(0); // проверяем, что в корзине остался один товар // проверяем, что в корзине остался 1 товар
    cy.get(".basket-item__controls-buttons .el-button.el-button--small.is-text")
      .eq(1)
      .click(); // удаляем товар для следующего теста
    cy.wait(2000); //ожидание
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: как добавят в раздел "Корзина" модальное окно "Удалить товар" добавить его в тесты

  it("Test 8: Несколько товаров удяется из корзину при выборе чек-боксов (авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.wait(2000); //ожидание
    AddProductToBasket(3); //добавляем в корзину из каталога 3 позиции товара
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.wait(2000); //ожидание
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    cy.get(
      ".basket-section-items__top .el-checkbox.el-checkbox--small.is-checked"
    ).click(); // убираем чек-бокс "Выбрать все"
    cy.wait(4000); //ожидание
    ConfirmGeolocation();
    cy.get(".basket-section-items__item .el-checkbox__input").eq(0).click(); //отмечаем чек-боксом  товар
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__item .el-checkbox__input").eq(1).click(); //отмечаем чек-боксом товар
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем выбранные товары из корзины
    cy.wait(2000); //ожидание
    IsVisibleProductBasket(0); // проверяем, что в корзине остался один товар // проверяем, что в корзине остался 1 товар
    cy.get(".basket-item__controls-buttons .el-button.el-button--small.is-text")
      .eq(1)
      .click(); // удаляем товар для следующего теста
    cy.wait(2000); //ожидание
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: как добавят в раздел "Корзина" модальное окно "Удалить товар" добавить его в тесты

  it("Test 17: При выборе товаров в корзине страница переходит в раздел 'Оформление заказа' (не авторизован)", () => {
    AddProductToBasket(2); //добавляем 2 позиции товара в корзину из каталога
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    cy.wait(2000); //ожидание
    IsVisibleProductBasket(2); // выбранный товар из каталога добавлен и видимый в корзине
    cy.wait(2000); //ожидание
    cy.get(
      ".basket-section-summary .el-button.el-button--primary.el-button--large"
    ).click(); //нажать на кнопку "Оформление заказа"
    cy.wait(2000); //ожидание
    cy.get('.el-input__wrapper .el-input__inner[inputmode="numeric"]').type(
      validUser.registered_phone
    ); //поле для ввода номера телефона
    cy.get(".box__body .el-button.el-button--primary").click(); //нажатие на кнопку "Получить код"
    cy.wait(2000); //ожидание
    cy.get(".auth-box__body.content .el-input").type(validUser.valid_code); //поле для ввода кода из смс
    cy.url().should("eq", "https://test.pricehack.ru/checkout"); // проверяем, что URL изменился на нужный
    cy.wait(2000); //ожидание
    IsVisibleProductCheckout; // проверяем, что товар из корзины отобразился в оформлении заказа
    cy.wait(2000); //ожидание
    cy.get(".icon-and-text-link__icon")
      .eq(1)
      .should("exist")
      .should("be.visible")
      .click(); // нажимаем на "Корзина"
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.wait(2000); //ожидание
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: как добавят в раздел "Корзина" модальное окно "Удалить товар" добавить его в тесты (добавила задержку в 2 секунды,без нее товар не удаляется)

  it("Test 18: При выборе товаров в корзине страница переходит в раздел 'Оформление заказа' (авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    AddProductToBasket(2); //добавляем 2 позиции товара в корзину из каталога
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    cy.get(
      ".basket-section-summary .el-button.el-button--primary.el-button--large"
    ).click(); //нажать на кнопку "оформление заказа"
    cy.wait(2000); //ожидание
    cy.url().should("eq", "https://test.pricehack.ru/checkout"); // проверяем, что URL изменился на нужный
    IsVisibleProductCheckout(2); // проверяем, что товар из корзины отобразился в оформлении заказа
    cy.get(".header-bottom-right.hidden-lg-and-down .header-bottom-nav__item")
      .eq(1)
      .click(); // нажимаем на "Корзина"
    cy.wait(4000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.wait(2000); //ожидание
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: как добавят в раздел "Корзина" модальное окно "Удалить товар" добавить его в тесты (добавила задержку в 2 секунды,без нее товар не удаляется)

  it("Test 19: При добавлении товара в корзину не авторизованного пользователя после авторизации товар перемещается пользователю (не авторизован)", () => {
    AddProductToBasket(2); //добавляем 2 позиции товара в корзину из каталога
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.url().should("eq", "https://test.pricehack.ru/basket"); // проверяем, что URL изменился на нужный
    ConfirmGeolocation();
    IsVisibleProductBasket(2); // выбранный товар из каталога добавлен и видимый в корзине
    authorization();
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    IsVisibleProductBasket(2); // находим товар, который добавляли и проверяем что он видимый и есть в корзине
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.wait(2000); //ожидание
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: как добавят в раздел "Корзина" модальное окно "Удалить товар" добавить его в тесты (добавила задержку в 2 секунды,без нее товар не удаляется)
});
