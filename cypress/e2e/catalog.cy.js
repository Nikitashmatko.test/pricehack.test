import validUser from "/cypress/fixtures/validUser.json";
import { authorization } from "/cypress/support/utils";
import { AddProductToBasket } from "/cypress/support/utils";
import { IsVisibleProductBasket } from "/cypress/support/utils";
import { AddProductToFavorite } from "../support/utils";
import { IsVisibleProductFavorite } from "../support/utils";
import { ClickProduct } from "../support/utils";
describe("Страница раздела каталог", () => {
  beforeEach(() => {
    cy.visit("/catalog/vse-tovaryi");
  });
  it("Test 1: Отображение товаров при переходе в раздел каталог(не авторизован)", () => {
    cy.get(".products__list .products__item").should("have.length", 44); //товар отображается в разделе каталог
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 2: Отображение товаров при переходе в раздел каталог(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.get(".products__list .products__item").should("have.length", 44); //товар отображается в разделе каталог
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 3: Добавление товара в корзину(не авторизован)", () => {
    AddProductToBasket(1); //добавляем товар из каталога в корзину
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    IsVisibleProductBasket(1); //выбранный товар из каталога добавлен и видимый в корзине
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 4: Добавление товара в корзину(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.wait(2000); //ожидание
    AddProductToBasket(1); //добавляем товар из каталога в корзину
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    IsVisibleProductBasket(1); //выбранный товар из каталога добавлен и видимый в корзине
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 5: При добавлении товара в корзину, добавляется количества товара(не авторизован)", () => {
    AddProductToBasket(1); //добавляем товар из каталога в корзину
    cy.wait(2000); //ожидание
    cy.get(".el-input-number__increase").click(); // нажимаем + и добавляем 2 товара в корзину
    cy.get('.el-input__inner[type="number"]')
      .eq(2)
      .should("exist")
      .should("be.visible")
      .should("have.attr", "aria-valuenow", "2"); // проверяем, что количество товара изменилось на 2
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    IsVisibleProductBasket(1); //выбранный товар из каталога добавлен и видимый в корзине
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 6: При добавлении товара в корзину, добавляется количества товара(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.wait(2000); //ожидание
    AddProductToBasket(1); //добавляем товар из каталога в корзину
    cy.wait(2000); //ожидание
    cy.get(".el-input-number__increase").click(); // нажимаем + и добавляем 2 товара в корзину
    cy.get('.el-input__inner[type="number"]')
      .eq(2)
      .should("exist")
      .should("be.visible")
      .should("have.attr", "aria-valuenow", "2"); // проверяем, что количество товара изменилось на 2
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    IsVisibleProductBasket(1); //выбранный товар из каталога добавлен и видимый в корзине
    cy.wait(2000); //ожидание
    cy.get(".basket-section-items__top .el-button.is-text").click(); // удаляем все товары для следующего теста
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что корзина очистилась
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 7: Выбранный товар добавляется в 'Избранное'(не авторизован)", () => {
    AddProductToFavorite(1); //добавляем товар в избранное
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    IsVisibleProductFavorite(1); //в избранном отображается выбранный товар
    cy.get(".el-button.is-circle.is-text.product-card__favorite.is-active")
      .eq(0)
      .click(); // отменяем товар из избранного
    cy.reload(); //обновляем страницу
    cy.contains("В избранном пусто").should("be.visible"); // проверяем, что в избранном пусто
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 8: Выбранный товар добавляется в 'Избранное'(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.wait(2000); //ожидание
    AddProductToFavorite(1); //добавляем товар в избранное
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    IsVisibleProductFavorite(1); //в избранном отображается выбранный товар
    cy.get(".el-button.is-circle.is-text.product-card__favorite.is-active")
      .eq(0)
      .click(); // отменяем товар из избранного
    cy.reload(); //обновляем страницу
    cy.contains("В избранном пусто").should("be.visible"); // проверяем, что в избранном пусто
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 13: Пагинация страниц в каталоге (не авторизован)", () => {
    cy.get('.number[aria-label="page 2"]').click(); //нажимаем на 2 страницу
    cy.get(".products").should("exist").should("be.visible"); //товар отображается на второй странице
    cy.get(".btn-next.is-last").click(); //нажимаем на стрелку для для открытия следующей страницы
    cy.get(".products").should("exist").should("be.visible"); //товар отображается на третьей странице
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 14: Пагинация страниц в каталоге (авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.wait(2000); //ожидание
    cy.get('.number[aria-label="page 2"]').click(); //нажимаем на 2 страницу
    cy.get(".products").should("exist").should("be.visible"); //товар отображается на второй странице
    cy.get(".btn-next.is-last").click(); //нажимаем на стрелку для для открытия следующей страницы
    cy.get(".products").should("exist").should("be.visible"); //товар отображается на третьей странице
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 15: Просмотр выбранного товара (не авторизован)", () => {
    cy.wait(2000); //ожидание
    ClickProduct(1);
    cy.wait(2000); //ожидание
    cy.get(".product-section__main").should("exist").should("be.visible");
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 16: Просмотр выбранного товара (авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.wait(2000); //ожидание
    ClickProduct(1);
    cy.get(".product-section__main").should("exist").should("be.visible");
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами
});
