import { authorization } from "/cypress/support/utils";
import { AddAddressProfile } from "/cypress/support/utils";
describe("Страница Личные данные", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("Test 6: Выйти из аккаунта ", () => {
    authorization();
    cy.get(".el-avatar.el-avatar--circle.hidden-xl-and-down").click(); //открыть профиль
    cy.get(
      ".profile-controls__controls .el-button.el-button--small.is-text"
    ).click(); //нажать на кнопку "Выйти из аккаунта"
  });

  it.only("Test 9: Сохраненные адреса ", () => {
    authorization();
    cy.get(".el-avatar.el-avatar--circle.hidden-xl-and-down").click(); //открыть профиль
    AddAddressProfile();
    cy.get(".profile-addresses")
      .contains(
        "Москва, Ленинский проспект, кв. 1, этаж 2, подъезд 3, домофон 4"
      )
      .should("be.visible"); //адрес отображается в расзделе
    cy.get(".profile-addresses__radio-controls .el-button.is-circle.is-text")
      .eq(1)
      .click(); //удалить адрес
    cy.get(
      ".profile-addresses-delete-dialog__controls .el-button.el-button--primary"
    ).click(); //подтвердить удаление
  });
});
