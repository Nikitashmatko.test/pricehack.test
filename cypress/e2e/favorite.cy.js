import validUser from "/cypress/fixtures/validUser.json";
import { authorization } from "/cypress/support/utils";
import { AddProductToFavorite } from "../support/utils";
import { IsVisibleProductFavorite } from "../support/utils";
import { IsVisibleProductBasket } from "../support/utils";
describe("Страница раздела избранное", () => {
  beforeEach(() => {
    cy.visit("/catalog/vse-tovaryi");
  });
  it("Test 1: При удалении товара из избранного товар удаляется со страницы избранное(не авторизован)", () => {
    cy.wait(2000); //ожидание
    AddProductToFavorite(1); // находим товар и добавляем в избранное
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    cy.url().should("eq", "https://test.pricehack.ru/favorite"); // проверяем, что URL изменился на нужный
    IsVisibleProductFavorite(1); // товар отображается в избранном
    cy.get(".el-button.is-circle.is-text.product-card__favorite.is-active")
      .eq(0)
      .click(); // удаляем товар из избранного
    cy.reload(); //обновляем страницу
    cy.contains("В избранном пусто").should("be.visible"); // проверяем, что в избранном пусто
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 2: При удалении товара из избранного товар удаляется со страницы избранное(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.wait(2000); //ожидание
    AddProductToFavorite(1); // находим товар и добавляем в избранное
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное // нажимаем на избранное
    cy.wait(2000); //ожидание
    cy.url().should("eq", "https://test.pricehack.ru/favorite"); // проверяем, что URL изменился на нужный
    IsVisibleProductFavorite(1); // товар отображается в избранном
    cy.get(".el-button.is-circle.is-text.product-card__favorite.is-active")
      .eq(0)
      .click(); // удаляем товар из избранного
    cy.reload(); //обновляем страницу
    cy.contains("В избранном пусто").should("be.visible"); // проверяем, что в избранном пусто
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 3: При нажатии на товар на странице избранное, пользователь переходит на страницу просмотра товара(не авторизован)", () => {
    cy.wait(2000); //ожидание
    AddProductToFavorite(1); // находим товар и добавляем в избранное
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    cy.wait(2000); //ожидание
    cy.url().should("eq", "https://test.pricehack.ru/favorite"); // проверяем, что URL изменился на нужный
    cy.get(".el-card__body").eq(0).click(); //переходим в просмотр товара
    cy.wait(2000); //ожидание
    cy.get(".product-buy-block .el-button.el-button--large").eq(1).click(); // удаляем товар из избранного
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    cy.contains("В избранном пусто").should("be.visible"); // проверяем, что в избранном пусто
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 4: При нажатии на товар на странице избранное, пользователь переходит на страницу просмотра товара(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.wait(2000); //ожидание
    AddProductToFavorite(1); // находим товар и добавляем в избранное
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    cy.wait(2000); //ожидание
    cy.url().should("eq", "https://test.pricehack.ru/favorite"); // проверяем, что URL изменился на нужный
    cy.get(".el-card__body").eq(0).click(); //переходим в просмотр товара
    cy.wait(2000); //ожидание
    cy.get(".product-buy-block .el-button.el-button--large").eq(1).click(); // удаляем товар из избранного
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    cy.contains("В избранном пусто").should("be.visible"); // проверяем, что в избранном пусто
  }); // TODO: после того как починят раздел "КАТАЛОГ" добавить в начале переход на главную страницу сайта и затем переход в каталог с товарами

  it("Test 5: При добавлении товара в избранное неавторизованного пользователя, после авторизации или регистрации избранное перемещается пользователю", () => {
    cy.wait(2000); //ожидание
    AddProductToFavorite(1); // находим товар и добавляем в избранное
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    cy.wait(2000); //ожидание
    cy.url().should("eq", "https://test.pricehack.ru/favorite"); // проверяем, что URL изменился на нужный
    authorization();
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    cy.wait(2000); //ожидание
    IsVisibleProductFavorite(1); // товар отображается в избранном
    cy.get(".el-button.is-circle.is-text.product-card__favorite.is-active")
      .eq(0)
      .click(); // удаляем товар из избранного
    cy.reload(); //обновляем страницу
    cy.contains("В избранном пусто").should("be.visible"); // проверяем, что в избранном пусто
  });
  it("Test 6: При нажатии на кнопку 'Добавить в корзину' на странице избранное, товар добавляется в корзину(не авторизован)", () => {
    cy.wait(2000); //ожидание
    AddProductToFavorite(1); // находим товар и добавляем в избранное
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    cy.wait(2000); //ожидание
    cy.url().should("eq", "https://test.pricehack.ru/favorite"); // проверяем, что URL изменился на нужный
    cy.get(".el-card__body .el-button.el-button--primary").click(); //нажимаем на кнопку "В корзину"
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    cy.wait(2000); //ожидание
    IsVisibleProductBasket(1); //товар добавлен в корзину
    cy.get(".basket-item .el-button.el-button--small").eq(0).click(); //удаляем товар из избранного
    cy.get(".basket-item .el-button.el-button--small").eq(1).click(); //удаляем товар из корзины
    cy.reload(); //обновляем страницу
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что товар удалился
  });
  it("Test 7: При нажатии на кнопку 'Добавить в корзину' на странице избранное, товар добавляется в корзину(авторизован)", () => {
    cy.visit("https://test.pricehack.ru/");
    authorization();
    cy.visit("https://test.pricehack.ru/catalog/vse-tovaryi");
    cy.wait(2000); //ожидание
    AddProductToFavorite(1); // находим товар и добавляем в избранное
    cy.get('.icon-and-text-link[href="/favorite"]').click(); // нажимаем на избранное
    cy.wait(2000); //ожидание
    cy.url().should("eq", "https://test.pricehack.ru/favorite"); // проверяем, что URL изменился на нужный
    cy.wait(2000); //ожидание
    cy.get(".el-card__body .el-button.el-button--primary").click(); //нажимаем на кнопку "В корзину"
    cy.wait(2000); //ожидание
    cy.get('.icon-and-text-link[href="/basket"]').click(); // нажимаем на "Корзина"
    IsVisibleProductBasket(1); //товар добавлен в корзину
    cy.get(".basket-item .el-button.el-button--small").eq(0).click(); //удаляем товар из избранного
    cy.get(".basket-item .el-button.el-button--small").eq(1).click(); //удаляем товар из корзины
    cy.reload(); //обновляем страницу
    cy.contains("В корзине пока пусто").should("be.visible"); // проверяем, что товар удалился
  });
});
