const { defineConfig } = require("cypress");
const allureWriter = require("@shelex/cypress-allure-plugin/writer");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      allureWriter(on, config);
      return config;
    },
    baseUrl: "https://test.pricehack.ru",
  },
  // Добавляем команду для изменения разрешения экрана
  viewportWidth: 1920,
  viewportHeight: 1080,
});
